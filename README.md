# EPICS Alarm Server 

## Introduction

Contains the Docker and singularity files need to build a container containing 
Control System Studies (phoebus version) and run a kafka server needed to run 
the alarm-server. 


## Quickstart

Download the singularity image:


Run the singularity instance :

```
singularity instance.start kafka
```



## Software 

https://github.com/shroffk/phoebus

